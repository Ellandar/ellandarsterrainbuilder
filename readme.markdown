This is a voxel noise generation program reproducing the excellent works of Josh Tippetts in his article here:
http://www.gamedev.net/blog/33/entry-2249106-more-procedural-voxel-world-generation/
     Special thanks to Josh for his help (and patience) with ideas for resolving the lack of ExtendedTurbulence in libNoise.


The following utilities are used:

**Aubergines Noise:** http://forum.unity3d.com/threads/162158-Released-Aubergines-Noise
     This is a port of the libNoise C++ noise library: http://libnoise.sourceforge.net/
     For this to work, the updates I wrote for it: Gradient.cs and ExtendedTurbulence.cs are needed. They have been submitted
     inclusion in the main library.

**Cubiquity:** http://forum.unity3d.com/threads/180334-Voxels!-Introducing-Cubiquity-a-voxel-plugin-built-with-C-and-PolyVox
     A voxel engine built by David Williams at http://www.volumesoffun.com/

How to use:

**Creating a new project**

1. Create a new project

2. Download this code and place in project.

3. Take a copy of Cubiquity from: https://bitbucket.org/volumesoffun/cubiquity-for-unity3d or http://www.volumesoffun.com/downloads/Cubiquity/cubiquity-for-unity3d-preview05.zip

4. Take a copy of Aubergines Noise Library: http://tamer.co/AubNoise.rar

5. Create a new Scene and add 2 Empty game objects.   Call one GUIButon, and the other Scripts.

6. Add a directional Light to the scene.

7. Attach the script GUIButton to the GameObject called GUIButton.

8. Attach the script NoiseDemo to the GameObject called Scripts. 

Press play.