﻿using System.Collections.Generic;
using Aubergine.Noise;
using Aubergine.Noise.NoiseUtils;
using Cubiquity;
using UnityEngine;
using System.Collections;
using Gradient = Aubergine.Noise.Module.Gradient;

/// <summary>
/// Used to build cubic terrains from an IModule noise source
/// </summary>
public class CubiquityGenerator {

    // It's best to create these outside of the loop.
    private static QuantizedColor open = new QuantizedColor(0,0,0,0);
    private static QuantizedColor red = new QuantizedColor(255, 0, 0, 255);
    private static QuantizedColor blue = new QuantizedColor(0, 0, 255, 255);
    private static QuantizedColor gray = new QuantizedColor(127, 127, 127, 255);
    private static QuantizedColor yellow = new QuantizedColor(255, 255, 0, 255);
    private static QuantizedColor brown = new QuantizedColor(50, 100,0,255);
    private static QuantizedColor grass = new QuantizedColor(172, 120, 29, 255);
    private static QuantizedColor platinumSilver = new QuantizedColor(227,227,227,255);
    private static QuantizedColor white = new QuantizedColor(255,255,255,255);


    /// <summary>
    /// Generates a map based on a supplied noise
    /// </summary>
    /// <param name="mapToBuild">IModule noise map</param>
    /// <param name="mapDimentions">Size of the map</param>
    /// <param name="disableRocks">If rocks should be drawn.</param>
    /// <param name="disableIncrementalBuild">If map should yield after every row during generation.</param>
    /// <param name="sampleDimentions">The area of the provided noise to sample.</param>
    /// <param name="scale"></param>
    /// <returns></returns>
    public static GameObject Generate(IModule mapToBuild, Vector3 mapDimentions, bool disableRocks, bool disableIncrementalBuild, int scale = 64)
    {
        // Start with some empty volume data and we'll write our pat into this..
        ColoredCubesVolumeData data = ColoredCubesVolumeData.CreateEmptyVolumeData<ColoredCubesVolumeData>(new Region(0, 0, 0, (int)mapDimentions.x - 1, (int)mapDimentions.y - 1, (int)mapDimentions.z - 1));

        // Create the gameobject
        GameObject coloredCubesGameObject = ColoredCubesVolume.CreateGameObject(data,true,true);

        // At this point we have a volume created and can now start writting our map data into it.
        SpawnCoroutine.StartCoroutine(BuildTerrain(mapToBuild, mapDimentions, disableRocks, data, disableIncrementalBuild, scale));
       
        return coloredCubesGameObject;
    }

    /// <summary>
    /// Generates a terrain from a 2d Texture heightmap
    /// </summary>
    /// <param name="textureSource">2D Texture to build map from.</param>
    /// <param name="sampleResolution">How much the sampler zooms into texture</param>
    /// <param name="mapDimentions">How big the terrain is going to be. Width/Height/Depth</param>
    /// <param name="disableRocks">If rocks should be drawn.</param>
    /// <param name="disableIncrementalBuild">If map should yield after every row during generation.</param>
    /// <param name="offsetX">How far from 0,0 should the sampler begin on X axis.</param>
    /// <param name="offsetY">How far from 0,0 should the sampler begin on Y axis.</param>
    /// <returns></returns>
    public static GameObject Generate(Texture2D textureSource, Vector3 mapDimentions, bool disableIncrementalBuild, int offsetX = 0, int offsetY = 0)
    {
        // Start with some empty volume data and we'll write our pat into this..
        ColoredCubesVolumeData data = ColoredCubesVolumeData.CreateEmptyVolumeData<ColoredCubesVolumeData>(new Region(0, 0, 0, (int)mapDimentions.x - 1, (int)mapDimentions.y - 1, (int)mapDimentions.z - 1));

        // Create the gameobject
        GameObject coloredCubesGameObject = ColoredCubesVolume.CreateGameObject(data, true, true);

        // At this point we have a volume created and can now start writting our map data into it.
        SpawnCoroutine.StartCoroutine(BuildTerrain(textureSource, mapDimentions, data, disableIncrementalBuild, offsetX, offsetY));

        return coloredCubesGameObject;
    }

    /// <summary>
    /// Coroutine that builds the map from a IModule data source.
    /// </summary>
    /// <param name="mapToBuild"></param>
    /// <param name="mapDimentions"></param>
    /// <param name="disableRocks"></param>
    /// <param name="data"></param>
    /// <param name="disableIncrementalBuild"></param>
    /// <param name="sampleDimentions"></param>
    /// <param name="scale"></param>
    /// <returns></returns>
    static IEnumerator BuildTerrain(IModule mapToBuild, Vector3 mapDimentions, bool disableRocks, ColoredCubesVolumeData data, bool disableIncrementalBuild, int scale)
    {
        // the "[x,y,z]value stuff below zooms into the noise supplied, as noise is quite high resolution.
        // We really should lock this down to a 1x1x1 to ensure gradient stuff stays constant in the generators, but meh :)
        float inverseScale = 1.0f/scale;

        bool terrainComplete = false;
        while (!terrainComplete)
        {

            for (int x = 0; x < mapDimentions.x; x++)
            {
                float xvalue = x * inverseScale;

                for (int z = 0; z < mapDimentions.z; z++)
                {
                    float zvalue = z * inverseScale;
                    for (int y = 0; y < mapDimentions.y; y++)
                    {
//                        float yvalue = (1 / (float)mapDimentions.y) * y;
                        float yvalue = y * inverseScale;
                        QuantizedColor locationColor = GetColor(mapToBuild.GetValue(xvalue, zvalue, yvalue));
                        // If we are disabling rocks, draw open cubes instead of grey/red.
                        if (disableRocks && (locationColor.Equals(red) | locationColor.Equals(gray))) data.SetVoxel(x, y, z, open);
                        else data.SetVoxel(x, y, z, locationColor);
                    }
                }
                if(!disableIncrementalBuild)yield return new WaitForEndOfFrame();
            }
            terrainComplete = true;
        }
    }

    /// <summary>
    /// Coroutine that builds the map from a Texture2D data source.
    /// This uses a Greyscale texture (no colors), where black (0,0,0): = -1 and white(255,255,255):  = 1
    /// </summary>
    /// <param name="textureSource"></param>
    /// <param name="mapDimentions"></param>
    /// <param name="disableRocks"></param>
    /// <param name="data"></param>
    /// <param name="disableIncrementalBuild"></param>
    /// <param name="offsetX"></param>
    /// <param name="offsetY"></param>
    /// <returns></returns>
    static IEnumerator BuildTerrain(Texture2D textureSource,
        Vector3 mapDimentions,
        ColoredCubesVolumeData data,
        bool disableIncrementalBuild,
        int offsetX,
        int offsetY)
    {

        // First lets build a black and white gradient to sample the texture with.       
        bool terrainComplete = false;
        while (!terrainComplete)
        {
            if (!disableIncrementalBuild) yield return 0;
            for (int x = 0; x < mapDimentions.x; x++)
            {
                for (int z = 0; z < mapDimentions.z; z++)
                {
                    // Get the height at this x,y co-ord and then place voxels from 0 up to this height.
                    // Make the voxels the same color as the texture.
                    Color locationColor = textureSource.GetPixel(x + offsetX, z + offsetY);
                    int height = (int)(locationColor.grayscale*mapDimentions.y);

                    for (int y = 0; y < height; y++)
                    {
                        data.SetVoxel(x, y, z, (QuantizedColor)locationColor);

                    }
                }
                
            }
            terrainComplete = true;
        }
    }
    

    private static QuantizedColor GetColor(double mapData)
    {
        
        //  Debug.Log("Map value is " +mapData);
        if (mapData.Equals(1f)) return open;
        if (mapData.Equals(2)) return brown;
        if (mapData.Equals(3f)) return gray;
        if (mapData.Equals(4f)) return yellow;
        if (mapData.Equals(5f)) return blue;
        if (mapData.Equals(6f)) return platinumSilver;
        if (mapData.Equals(7f)) return grass;
        if (mapData.Equals(8f)) return white;
                
        return red;
    }

}




