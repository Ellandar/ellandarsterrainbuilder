﻿using System;
using UnityEngine;
using System.Collections;

public class SpawnCoroutine {


    /// <summary>
    /// Spawns a GameObject and attaches a corotine component to it.
    /// It then passes the Corotine into it, this allows me to spawn a coroutine out of
    /// static and non-monobehavour classes.
    /// </summary>
    /// <param name="coroutineToSpawn"></param>
    /// <returns></returns>
	public static Coroutine StartCoroutine(IEnumerator coroutineToSpawn)
    {
        if (coroutineToSpawn == null) throw new ArgumentNullException("coroutineToSpawn","SpawnCoroutine was not supplied a coroutine to spawn");

        // Spawn a container object to hold all co-rotines if needed
        GameObject routineContainer = GameObject.Find("Coroutines Are Here");
        if(!routineContainer) { routineContainer = new GameObject("Coroutines Are Here");}

        // Spawn the gameobject that will host our coroutine
        GameObject coroutineHost = new GameObject("Coroutine Host");
        coroutineHost.transform.parent = routineContainer.transform;

        // Creat a monobehaviour component that will process our coroutine.
        CoroutineProcessor coroutineComponent = coroutineHost.AddComponent<CoroutineProcessor>();

        // Tell the component to start working, damnnit (and return the result when done).
        return coroutineComponent.StartWork(coroutineToSpawn);
    }

}

/// <summary>
/// Monobehaviour to add to a gameobject that will host the coroutine.
/// </summary>
public class CoroutineProcessor : MonoBehaviour
{
    /// <summary>
    /// Call this and pass in the coroutine you want to run.
    /// It will destroy the gameobject when the co-routine is finished.
    /// </summary>
    /// <param name="coroutineToSpawn"></param>
    /// <returns></returns>
    public Coroutine StartWork(IEnumerator coroutineToSpawn)
    {
        return StartCoroutine(BeatingHeart(coroutineToSpawn));
        
    }

    /// <summary>
    /// Finally, the thing that runs the coroutine :)
    /// </summary>
    /// <param name="coroutineToSpawn"></param>
    /// <returns></returns>
    private IEnumerator BeatingHeart(IEnumerator coroutineToSpawn)
    {
        yield return StartCoroutine(coroutineToSpawn);
        Destroy(gameObject);
    }

}

