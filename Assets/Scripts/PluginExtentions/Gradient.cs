﻿using System;
using UnityEngine;

namespace Aubergine.Noise.Module
{
    public class Gradient : IModule
    {
        //Properties//
        public IModule Module0 { get; set; }
        public Vector3 LowerBound { get; set; }
        public Vector3 UpperBound { get; set; }
        
        // Vector start point.
        public double X1 { get; set; }
        public double Y1 { get; set; }
        public double Z1 { get; set; }

        // Probably don't need these 3 as they can be calculated if needed.
        public double X2 { get; set; }
        public double Y2 { get; set; }
        public double Z2 { get; set; }

        // Vector magnitude and components
        public double VectorLength { get; set; }
        public double VectorxLength { get; set; }
        public double VectoryLength { get; set; }   
        public double VectorzLength { get; set; }
        
        //Constructors//
        public Gradient(double x1, double y1, double z1, double x2, double y2, double z2)
        {
            //Module0 = mod0;
            X1 = x1;
            Y1 = y1;
            Z1 = z1;
            X2 = x2;
            Y2 = y2;
            Z2 = z2;
            
            // Determine the vector components.
            VectorxLength = X2 - X1;
            VectoryLength = Y2 - Y1;
            VectorzLength = Z2 - Z1;

            // determine the magnitued of the vector.
            VectorLength = VectorxLength*VectorxLength + VectoryLength*VectoryLength + VectorzLength*VectorzLength;

            // Bit of logging  Careful not to do this with a full terrain (unless you have a couple of hours to spare for logging) :)
//            Debug.Log("VectorLength = "+VectorLength);
//            Debug.Log("Vectorx = "+VectorxLength+" VectorY = "+VectoryLength+" VectorZ = "+VectorzLength);

        }

        public double GetValue(double x, double y, double z)
        {
            double dx = x - X1;
            double dy = y - Y1;
            double dz = z - Z1;
            double dp = dx * VectorxLength + dy * VectoryLength + dz * VectorzLength;
            dp = Mathf.Clamp((float)dp / (float)VectorLength,0f,1.0f);
            //Debug.Log("dx = "+dx+" dy = "+dy+" dz = "+dz+" dp = "+dp+" lerp = "+Mathf.Lerp(-1.0f, 1.0f, (float) dp));
            return Mathf.Lerp(-1.0f, 1.0f, (float) dp);
            
        }

    }

   

}