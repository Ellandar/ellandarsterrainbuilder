﻿using Aubergine.Noise;
using Aubergine.Noise.Module;
using UnityEngine;
using System.Collections;

public class ExtendedTurbulence : IModule {

		//Variables//
		IModule xDistortModule;
		IModule yDistortModule;
		IModule zDistortModule;

		//Properties//
		public IModule Module0 { get; set; }
		public double Power { get; set; }

    public IModule XDistortModule
    {
        get { return xDistortModule; }
        set { xDistortModule = value; }
    }

    public IModule YDistortModule
    {
        get { return yDistortModule; }
        set { yDistortModule = value; }
    }

    public IModule ZDistortModule
    {
        get { return zDistortModule; }
        set { zDistortModule = value; }
    }

		//Constructor//
		public ExtendedTurbulence() {
			xDistortModule = new Perlin();
			yDistortModule = new Perlin();
			zDistortModule = new Perlin();
		}
		public ExtendedTurbulence(IModule mod0) {
			Module0 = mod0;
			xDistortModule = new Perlin();
			yDistortModule = new Perlin();
			zDistortModule = new Perlin();

			Power = 1.0;

		}

    public ExtendedTurbulence(IModule mod0, IModule suppliedxDistortModule, IModule suppliedyDistortModule, IModule suppliedzDistortModule)
    {
        Module0 = mod0;
        xDistortModule = suppliedxDistortModule;
        yDistortModule = suppliedyDistortModule;
        zDistortModule = suppliedzDistortModule;

        Power = 1.0;
    }

    public void SetConstantTurb(Const constModule)
    {
        xDistortModule = constModule;
        yDistortModule = constModule;
        zDistortModule = constModule;
    }

    //Methods//
		public double GetValue(double x, double y, double z) {
			//Get values from three Perlins
			double x0, y0, z0;
			double x1, y1, z1;
			double x2, y2, z2;
			x0 = x + (12414.0 / 65536.0);
			y0 = y + (65124.0 / 65536.0);
			z0 = z + (31337.0 / 65536.0);
			x1 = x + (26519.0 / 65536.0);
			y1 = y + (18128.0 / 65536.0);
			z1 = z + (60493.0 / 65536.0);
			x2 = x + (53820.0 / 65536.0);
			y2 = y + (11213.0 / 65536.0);
			z2 = z + (44845.0 / 65536.0);
			double xDistort = x + (xDistortModule.GetValue(x0, y0, z0) * Power);
			double yDistort = y + (yDistortModule.GetValue(x1, y1, z1) * Power);
			double zDistort = z + (zDistortModule.GetValue(x2, y2, z2) * Power);
			//Retrieve the output value
			return Module0.GetValue(xDistort, yDistort, zDistort);
		}
	}



