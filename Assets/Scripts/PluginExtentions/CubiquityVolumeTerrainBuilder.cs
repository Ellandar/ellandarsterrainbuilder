﻿using System.Collections.Generic;
using Aubergine.Noise;
using Cubiquity;
using UnityEngine;


public static class CubiquityVolumeTerrainBuilder
{


    public static GameObject GenerateVolume(TerrainVolumeData inData, Region inRegion)
    {
        return new GameObject();
    }


    public static GameObject GenerateVolume(IModule suppliedNoise, Vector3 mapSize, bool DisableRocks, bool disableIncrementalBuild, bool SmoothNonCubicTerrain, int scale = 64)
    {
        Region myRegion = new Region(0, 0, 0, (int) mapSize.x, (int) mapSize.y, (int) mapSize.z);
        
        TerrainVolumeData data = TerrainVolumeData.CreateEmptyVolumeData<TerrainVolumeData>(myRegion);
        string volumeDataPath = data.fullPathToVoxelDatabase;
        // Create some ground in the terrain so it shows up in the editor.
        // Soil as a base (mat 1) and then a couple of layers of grass (mat 2).
        //TerrainVolumeGenerator.GenerateFloor(data, 2, (uint)1, 3, (uint)2);

        // Now create the terrain game object from the data.
        GameObject terrain = TerrainVolume.CreateGameObject(data,true,true);

        // Set up our material	
        Material material = new Material(Shader.Find("Triplanar"));
        terrain.GetComponent<TerrainVolumeRenderer>().material = material;

        // Set up the default textures
        Texture2D rockTexture = Resources.Load("Textures/Rock (Basic)") as Texture2D;
        Texture2D soilTexture = Resources.Load("Textures/GoodDirt") as Texture2D;
        Texture2D grassTexture = Resources.Load("Textures/Grass (Meadows2)") as Texture2D;
        Texture2D goldTexture = Resources.Load("Textures/Sand (with pebbles)") as Texture2D;
        Texture2D ironOreTexture = Resources.Load("Textures/IronOre") as Texture2D;

        // It's possible the textures won't actually be found, as they are just examples and the
        // user might have decided not to include them when importing Cubiquity. This doesn't
        // matter and just means the uer will have to set up their own textures.
        if (rockTexture != null && soilTexture != null && grassTexture != null)
        {
            material.SetTexture("_Tex0", rockTexture);
            material.SetTextureScale("_Tex0", new Vector2(0.062f, 0.062f));
            material.SetTexture("_Tex1", soilTexture);
            material.SetTextureScale("_Tex1", new Vector2(0.125f, 0.125f));
            material.SetTexture("_Tex2", grassTexture);
            material.SetTextureScale("_Tex2", new Vector2(0.125f, 0.125f));
            material.SetTexture("_Tex3", goldTexture);
            material.SetTextureScale("_Tex3", new Vector2(0.125f, 0.125f));
            material.SetTexture("_Tex4", ironOreTexture);
            material.SetTextureScale("_Tex4", new Vector2(0.125f, 0.125f));

        }

        // Setup the materials
        MaterialSet materialSet = new MaterialSet();
        // Ok, lets get a-buildin!

        // This scale factor comtrols the size of the rocks which are generated.
        float inverseScale = 1.0f/scale;

        int height = myRegion.upperCorner.y;
        int depth = myRegion.upperCorner.z;
        int width = myRegion.upperCorner.x;
        // Iterate over every voxel of our volume
        for (int x = 0; x < width; x++)
        {
            float sampleX = x*inverseScale;
            
            for (int z = 0; z < depth; z++)
            {
                float sampleZ = z*inverseScale;
                for (int y = 0; y < height; y++)
                {
                    float sampleY = (1/(float) height)*y;

                    // Make sure we don't have anything left in here from the previous voxel
                    materialSet.weights[0] = 0;
                    materialSet.weights[1] = 0;
                    materialSet.weights[2] = 0;
                    materialSet.weights[3] = 0;
                    materialSet.weights[4] = 0;

                    // Get the voxel value at this point.
                    double simplexNoiseValue = suppliedNoise.GetValue(sampleX, sampleZ, sampleY);


                    //Set the material based upon the return above.

                    if (simplexNoiseValue.Equals(2)) materialSet.weights[2] = (byte) 255;
                    if (simplexNoiseValue.Equals(3)) materialSet.weights[1] = (byte) 255;
                    if (simplexNoiseValue.Equals(4)) materialSet.weights[3] = (byte) 255;
                    if (simplexNoiseValue.Equals(5)) materialSet.weights[0] = (byte) 255;
                    if (simplexNoiseValue.Equals(6)) materialSet.weights[0] = (byte) 255;


                    // write the cube.
                    data.SetVoxel(x, y, z, materialSet);
                }
            }
        }

        // Now lets do a second pass to smooth everything.  Every voxel will be smoothed by averaging their weight by the surrounding voxels materials.
        if (SmoothNonCubicTerrain)
        {
            data.CommitChanges();
            //data.SaveVolume();
            
            TerrainVolumeData duplicateVolumeData = TerrainVolumeData.CreateFromVoxelDatabase<TerrainVolumeData>(volumeDataPath,VolumeData.WritePermissions.ReadWrite);
            
            List<float> totalSurroundingWeight = new List<float>(8);
            for (int i = 0; i < 8; i++)
            {
                totalSurroundingWeight.Add(0f);
            }

            for (int x = 0; x < width; x++)
            {
                for (int z = 0; z < depth; z++)
                {
                    for (int y = 0; y < height; y++)
                    {


                        // ok, lets now look around this voxel for neighbours and start averaging.
                        for (int xDisplace = -1; xDisplace <= 1; xDisplace++)
                        {
                            for (int zDisplace = -1; zDisplace <= 1; zDisplace++)
                            {
                                for (int yDisplace = -1; yDisplace <= 1; yDisplace++)
                                {
                                    // Skip the current voxel.
                                    if (xDisplace == 0 && zDisplace == 0 && yDisplace == 0) continue;

                                    // Get the materials from the voxel
                                    MaterialSet displacedVoxelMaterialSet = duplicateVolumeData.GetVoxel(xDisplace + x, yDisplace + y, zDisplace + z);

                                    // Iterate through all the materials adding up their weights.
                                    for (int materialIndex = 0; materialIndex < displacedVoxelMaterialSet.weights.Length; materialIndex++)
                                    {
                                        totalSurroundingWeight[materialIndex] += displacedVoxelMaterialSet.weights[(uint) materialIndex];

                                    }
                                }
                            }
                        }

                        // finally, set the voxels materials weight to the average of everything around it.
                        MaterialSet currentVoxelMaterialSet = data.GetVoxel(x, y, z);
                        for (int materialIndex = 0; materialIndex < currentVoxelMaterialSet.weights.Length; materialIndex++)
                        {
                            if (totalSurroundingWeight[materialIndex] > 0)
                            {
                                currentVoxelMaterialSet.weights[(uint) materialIndex] = (byte) (totalSurroundingWeight[materialIndex]/26);
                                totalSurroundingWeight[materialIndex] = 0;
                            }
                        }


                        // Set the voxel
                        data.SetVoxel(x, y, z, currentVoxelMaterialSet);
                   
                    }
                }
            }
        }
        return terrain;
    }



    public static GameObject GenerateVolumeFromHeightMap(Region inRegion, IModule suppliedNoise)
    {
        TerrainVolumeData data = TerrainVolumeData.CreateEmptyVolumeData<TerrainVolumeData>(inRegion);

        // Create some ground in the terrain so it shows up in the editor.
        // Soil as a base (mat 1) and then a couple of layers of grass (mat 2).
        //TerrainVolumeGenerator.GenerateFloor(data, 2, (uint)1, 3, (uint)2);

        // Now create the terrain game object from the data.
        GameObject terrain = TerrainVolume.CreateGameObject(data,true,true);

        // Set up our material	
        Material material = new Material(Shader.Find("TriplanarTexturing"));
        terrain.GetComponent<TerrainVolumeRenderer>().material = material;

        // Set up the default textures
        Texture2D rockTexture = Resources.Load("Textures/Rock") as Texture2D;
        Texture2D soilTexture = Resources.Load("Textures/Soil") as Texture2D;
        Texture2D grassTexture = Resources.Load("Textures/Grass") as Texture2D;

        // It's possible the textures won't actually be found, as they are just examples and the
        // user might have decided not to include them when importing Cubiquity. This doesn't
        // matter and just means the uer will have to set up their own textures.
        if (rockTexture != null && soilTexture != null && grassTexture != null)
        {
            material.SetTexture("_Tex0", rockTexture);
            material.SetTextureScale("_Tex0", new Vector2(0.062f, 0.062f));
            material.SetTexture("_Tex1", soilTexture);
            material.SetTextureScale("_Tex1", new Vector2(0.125f, 0.125f));
            material.SetTexture("_Tex2", grassTexture);
            material.SetTextureScale("_Tex2", new Vector2(0.125f, 0.125f));
        }

        // Setup the materials
        MaterialSet materialSet = new MaterialSet();
        // Ok, lets get a-buildin!

        // This scale factor comtrols the size of the rocks which are generated.
        float rockScale = 32f;
        float invRockScale = 1.0f / rockScale;

        int height = inRegion.upperCorner.y;
        int depth = inRegion.upperCorner.z;
        int width = inRegion.upperCorner.x;

        // Iterate over every voxel of our volume
        for (int z = 0; z < depth; z++)
        {
            for (int y = height - 1; y > 0; y--)
            {
                for (int x = 0; x < width; x++)
                {

                    // Make sure we don't have anything left in here from the previous voxel
                    materialSet.weights[0] = 0;
                    materialSet.weights[1] = 0;
                    materialSet.weights[2] = 0;

                    // Simplex noise is quite high frequency. We scale the sample position to reduce this.
                    float sampleX = (float)x * invRockScale;
                    float sampleZ = (float)z * invRockScale;

                    double simplexNoiseValue = suppliedNoise.GetValue(sampleX, 0, sampleZ);
                    //Debug.Log("Unmodified Noise value at x =" + x + ", x = " + z + " is: " + simplexNoiseValue);
                    // FIXME simplexNoiseValue = Math3d.ConvertRange(-1, 1, 3, height - 1, simplexNoiseValue);
                    //Debug.Log("Modified Noise value at x =" + x + ", x = " + z + " is: " + simplexNoiseValue);

                    if (y < 1)
                    {
                        // Add to rock material channel.
                        materialSet.weights[0] = 255;
                    }
                    else if (y < simplexNoiseValue - 1)
                    {
                        // Add to soil material channel.
                        materialSet.weights[1] = 255;
                    }
                    else if (y < simplexNoiseValue)
                    {
                        // Add to grass material channel.
                        materialSet.weights[2] = 255;
                    }


                    // We can now write out computed voxel value into the volume.
                    data.SetVoxel(x, y, z, materialSet);
                }
            }
        }

        return terrain;

    }

}
