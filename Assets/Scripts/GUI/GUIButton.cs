﻿using UnityEngine;
using System.Collections;

public class GUIButton : MonoBehaviour {

    /// <summary>
    /// Draw the button to recreate the map.
    /// </summary>
    void OnGUI()
    {
        if (GUI.Button(new Rect(10, 10, 150, 30), "Regenerate Map"))
            FindObjectOfType<NoiseDemo>().BuildMap();
    }
}
