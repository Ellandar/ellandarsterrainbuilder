﻿using Aubergine.Noise.Module;
using UnityEngine;
using Gradient = Aubergine.Noise.Module.Gradient;


/// <summary>
/// Created by Robert Thompson
/// First Relased: 17 - Feb - 2014
/// At: 
/// 
/// 
/// This is a voxel noise generation program reproducing the excelent works of Josh Tippetts in his article here:
/// http://www.gamedev.net/blog/33/entry-2249106-more-procedural-voxel-world-generation/
///     Special thanks to Josh for his help (and patience) with ideas for resolving the lack of ExtendedTurbulence in libNoise.
/// 
/// 
/// The following utilities are used:
/// Aubergines Noise:http://forum.unity3d.com/threads/162158-Released-Aubergines-Noise
///     This is a port of the libNoise C++ noise library: http://libnoise.sourceforge.net/
///     For this to work, the updates I wrote for it: Gradient.cs and ExtendedTurbulence.cs are needed. They have been submitted
///     inclusion in the main library.
///    
/// Cubiquity: http://forum.unity3d.com/threads/180334-Voxels!-Introducing-Cubiquity-a-voxel-plugin-built-with-C-and-PolyVox
///     A voxel engine built (mainly) by David Williams at http://www.volumesoffun.com/
/// 
/// </summary>
public class NoiseDemo : MonoBehaviour
{


    [Range(0f, 2f)] public float SemiRareDensity = .3f;         // Higher number = more yellow ore
    [Range(0f, 2f)] public float RareGradientScale = 1f;        // Higher number = higher spread of blue ore
    [Range(0f, 2f)] public float RareDensity = .65f;            // Higher number = more blue ore
    [Range(0f, 2f)] public float DirtDepth = .2f;               // Higher number = more voxels of dirt layer (top layer)
    [Range(0f, 2000f)] public int GroundSeed = 1;                  // Any integer, acts as a randomiser for the terrain.
    [Range(0f, 2f)] public float GroundZScale = .2f;            // High number = rougher terrain.
    [Range(0f, 2f)] public float BedrockThreshold = 1;          // Higher number = more bedrock (bottom layer)
    [Range(0f, 2f)] public float CaveSize = .8f;                // Higher number = bigger caves.

    public int MapDepth = 128;                  // Map Dimentions
    public int MapWidth = 128;                  // Map Dimentions
    public int MapHeight = 50;                  // Map Dimentions
    public bool DisableRocks = false;           // True = no grey cubes will be rendered, allowing you to see underground.
    public bool DisableCaves = true;           // True = no caves will be carved from terrain.
    public bool DisableIncrementalBuild = false;// True = build entire map in single update, game will appear to freeze whilst map builds.
    public bool CreateCubicVolume = true;      // True = Create a cubic voxel world, False = Create a smooth terrain world.
    public bool SmoothNoncubicTerrain = false;  // True = run a second pass over the VolumeTerrain to smooth out the surface.

    private GameObject existingMap;


    public void Start()
    {
        BuildMap();
    }


    /// <summary>
    /// Call this at runtime and it will build a new map and destory the previous one.
    /// </summary>
    public void BuildMap()
    {

        //-----------------------------------------------Building the Ground Layer Functions-----------------------------------------------


        Const open = new Const { ConstantValue = 1f };
        Const dirt = new Const { ConstantValue = 2f };
        Const stone = new Const { ConstantValue = 3f };
        Const semiRare = new Const { ConstantValue = 4f };
        Const rare = new Const { ConstantValue = 5f };
        Const bedrock = new Const { ConstantValue = 6f };

        Const constant1 = new Const { ConstantValue = 1 };
        Const constant0 = new Const { ConstantValue = 0 };
        
        // Gradient
        // Remember, the noise module uses xy = ground plane, z = vertical. so keep that in mind.  Y in unity = Z in Gradient.
        Gradient mainGradient = new Gradient(0, 0, .5, 0, 0, 0);
        ScaleBias mainGradientRemap = new ScaleBias(mainGradient, 0.5, 0.5);

        // Semi-Rare ores
        Perlin semiRarePerlin = new Perlin { OctaveCount = 4, Frequency = 2 };
        ScaleBias semiRarePerlinRemap = new ScaleBias(semiRarePerlin, 0.5f, 0.5f);
        Select semiRareSelect = new Select(semiRare, stone, semiRarePerlinRemap);
        // stone is chosen when the number falls between Semirare density and 1000. Smaller numbers numbers = less semi-rare.
        semiRareSelect.SetBounds(SemiRareDensity, 1000);


        // Rare ores
        Perlin rarePerlin = new Perlin { OctaveCount = 3, Frequency = 3 };
        ScaleBias rarePerlinRemap = new ScaleBias(rarePerlin, 0.5f, 0.5f);
        ScaleBias rarePerlinScale = new ScaleBias(rarePerlinRemap, RareGradientScale, 0);
        Multiply rareMulti = new Multiply(rarePerlinScale, mainGradientRemap);
        ScaleBias rareMultiScale = new ScaleBias(rareMulti, RareDensity, 0);
        Select rareSelect = new Select(semiRareSelect, rare, rareMultiScale);
        rareSelect.SetBounds(.5, 1000);


        // Dirt Layer
        Select dirtStoneSelect = new Select(dirt, rareSelect, mainGradientRemap);
        dirtStoneSelect.SetBounds(DirtDepth, 1000);

        // set anything above dirtlayer as air.
        Select groundSelect = new Select(open, dirtStoneSelect, mainGradientRemap);
        groundSelect.SetBounds(0.000001, 1000);


        //-----------------------------------------------Applying Ground Shape-----------------------------------------------

        // Create noise to displace the ground.
        Perlin groundShape = new Perlin {Frequency = 2, Lacunarity = 1.4, OctaveCount = 3, Seed = GroundSeed};
        ScaleBias groundBias = new ScaleBias(groundShape, .35, -.25);
        ScalePoint groundZScale = new ScalePoint(groundBias) { ZScale = GroundZScale };
        ExtendedTurbulence groundTurb = new ExtendedTurbulence(groundSelect);
        groundTurb.SetConstantTurb(constant0);
        groundTurb.ZDistortModule = groundZScale;
        groundTurb.Power = 0.5f;



      
        //-----------------------------------------------Bringing back the caves-----------------------------------------------
        RidgedMulti caveShape1 = new RidgedMulti{Frequency = 2,OctaveCount = 1};
        Select caveBase1 = new Select(constant0, constant1,caveShape1);
        caveBase1.SetBounds(CaveSize,1000);

        RidgedMulti caveShape2 = new RidgedMulti { Frequency = 2, OctaveCount = 1, Seed = 1323};
        Select caveBase2 = new Select(constant0, constant1, caveShape2);
        caveBase1.SetBounds(1- CaveSize, 1000);
        Multiply caveMult = new Multiply(caveBase1,caveBase2);

        Perlin caveTurbX = new Perlin { OctaveCount = 3, Frequency = 3, Seed = 1001 };
        Perlin caveTurbY = new Perlin { OctaveCount = 3, Frequency = 3, Seed = 1201 };
        Perlin caveTurbZ = new Perlin { OctaveCount = 3, Frequency = 3, Seed = 1301 };
        ExtendedTurbulence caveTurb = new ExtendedTurbulence(caveMult, caveTurbX, caveTurbY, caveTurbZ);
        caveTurb.Power = .25;

        ScaleBias caveInvert = new ScaleBias(caveTurb, -1, 1);
        Select caveSelect = new Select(open, groundTurb, caveInvert,0,.5,1000);

        // Draw the bedrock, so there's less people falling into the abyss :)
        Select bedrockSelect;
        if (!DisableCaves)
        {
            bedrockSelect = new Select(caveSelect, bedrock, mainGradientRemap);
            bedrockSelect.SetBounds(BedrockThreshold, 1000);
        }
        else
        {
            bedrockSelect = new Select(groundTurb, bedrock, mainGradientRemap);
            bedrockSelect.SetBounds(BedrockThreshold, 1000);
        }

        // If a map already exists, destory it and then send this off to the generator to draw the map in voxels.
        if (existingMap) DestroyObject(existingMap);

        if (CreateCubicVolume)
        {
            existingMap = CubiquityGenerator.Generate(bedrockSelect, new Vector3(MapDepth, MapHeight, MapWidth), DisableRocks, DisableIncrementalBuild);
        }
        else
        {
            existingMap = CubiquityVolumeTerrainBuilder.GenerateVolume(bedrockSelect, new Vector3(MapDepth, MapHeight, MapWidth), DisableRocks, DisableIncrementalBuild,SmoothNoncubicTerrain);
        }


    }
}
