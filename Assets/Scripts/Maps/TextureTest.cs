﻿using Aubergine.Noise;
using Aubergine.Noise.Module;
using Aubergine.Noise.NoiseUtils;
using UnityEngine;
using System.Collections;

public class TextureTest : MonoBehaviour
{

    [Range(0,10)]   public float GroundFrequency = 1.25f;
    [Range(0,10)]   public int GroundOctaves = 6;
    [Range(0, 10)]  public float GroundPersistence = .5f;
    [Range(1,4)]    public float GroundLacunairty = 1.85f;
    [Range(0,100000)] public int GroundSeed = 0;

    [Range(-1, 1)] public float GroundHeightDisplacement = 0f;
    [Range(-1, 1)] public float GroundScale = .75f;
    public int MapWidth = 256;
    public int MapDepth = 256;
    public int MapHeight = 50;

    public double TextureSampleWidth = 1;
    public double TextureSampleHeight = 1;

    public bool DisableIncrementalBuild = true;

    private Texture2D textureToRender;
    private GameObject existingMap;
	// Use this for initialization
	void Start () {
	BuildMap();
	    
	}


    /// <summary>
    /// Call this at runtime and it will build a new map and destory the previous one.
    /// </summary>
    public void BuildMap()
    {

        // Create very basic noise.
        Perlin groundBillow = new Perlin { Frequency = GroundFrequency,OctaveCount = GroundOctaves,Persistence = GroundPersistence, Lacunarity = GroundLacunairty,Seed = GroundSeed};
        ScaleBias groundScaleBias = new ScaleBias(groundBillow, GroundScale, GroundHeightDisplacement);




        
        // Create a terrain map.
        NoiseMapBuilderPlane terrainBuilder = new NoiseMapBuilderPlane(MapWidth, MapDepth);
        terrainBuilder.SetBounds(0, TextureSampleHeight, 0, TextureSampleWidth);
        terrainBuilder.Build(groundScaleBias);

        // Build a renderer (to show the terrain that is to be built)
        RendererImage render = new RendererImage();
        render.SourceNoiseMap = terrainBuilder.Map;
        render.ClearGradient();
        // We want greyscale, so black to white gradients are added.
        render.AddGradientPoint(-1,Color.black);
        render.AddGradientPoint(1,Color.white);
        render.Render();

        textureToRender = render.GetTexture();


        // Send the texture to the voxel generator.

        if (existingMap) DestroyObject(existingMap);
        existingMap = CubiquityGenerator.Generate(textureToRender, new Vector3(MapDepth, MapHeight, MapWidth), DisableIncrementalBuild);

    }

    void OnGUI()
    {

        if (textureToRender != null)
            GUI.DrawTexture(new Rect(0, 30, textureToRender.width, textureToRender.height), textureToRender, ScaleMode.ScaleToFit, true);

        if (GUI.Button(new Rect(0, 0, 150, 30), "Regenerate Map"))
            BuildMap();
    }
}
