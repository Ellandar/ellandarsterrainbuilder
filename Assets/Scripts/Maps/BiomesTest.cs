﻿using System.Diagnostics;
using Aubergine.Noise;
using Aubergine.Noise.Module;
using Aubergine.Noise.NoiseUtils;
using UnityEngine;

public class BiomesTest : MonoBehaviour {

    [Range(0f, 2f)]
    public float SemiRareDensity = .3f;         // Higher number = more yellow ore
    [Range(0f, 2f)]
    public float RareGradientScale = 1f;        // Higher number = higher spread of blue ore
    [Range(0f, 2f)]
    public float RareDensity = .65f;            // Higher number = more blue ore
    [Range(0f, 2f)]
    public float DirtDepth = .2f;               // Higher number = more voxels of dirt layer (top layer)
    [Range(0f, 2000f)]
    public int GroundSeed = 1;                  // Any integer, acts as a randomiser for the terrain.
    [Range(0f, 2f)]
    public float GroundZScale = .25f;            // High number = rougher terrain.
    [Range(0f, 2f)]
    public float BedrockThreshold = 1;          // Higher number = more bedrock (bottom layer)
    [Range(0f, 2f)]
    public float CaveSize = .8f;                // Higher number = bigger caves.

    [Range(-1, 1)] public float MountainsVsHills;              // 0 = All mountains, 1 = All hills.
    [Range(-1, 1)] public float MountainhillsVsPlains;         // 0 = All mountains and hills, 1 = all plains.
    [Range(0, 5f)] public float EdgeFallOff;
    public int MapDepth = 128;                  // Map Dimentions
    public int MapWidth = 128;                  // Map Dimentions
    public int MapHeight = 50;                  // Map Dimentions
    public bool DisableRocks = false;           // True = no grey cubes will be rendered, allowing you to see underground.
    public bool DisableCaves = true;           // True = no caves will be carved from terrain.
    public bool DisableIncrementalBuild = true;// True = build entire map in single update, game will appear to freeze whilst map builds.
    public bool CreateCubicVolume = true;      // True = Create a cubic voxel world, False = Create a smooth terrain world.
    public bool SmoothNoncubicTerrain = false;  // True = run a second pass over the VolumeTerrain to smooth out the surface.


    private float TimetoBuild;
    private GameObject existingMap;
    private Texture2D textureToRender;

    public void Start()
    {

        BuildMap();
    }

    public void BuildMap()
    {
        Stopwatch timer = new Stopwatch();
        timer.Start();
        TimedBuildMap();
        timer.Stop();
        TimetoBuild = timer.ElapsedMilliseconds/1000f;
    }

    /// <summary>
    /// Call this at runtime and it will build a new map and destory the previous one.
    /// </summary>
    public void TimedBuildMap()
    {

        //-----------------------------------------------Building the Ground Layer Functions-----------------------------------------------


        Const open = new Const { ConstantValue = 1f };
        Const dirt = new Const { ConstantValue = 2f };
        Const stone = new Const { ConstantValue = 3f };
        Const semiRare = new Const { ConstantValue = 4f };
        Const rare = new Const { ConstantValue = 5f };
        Const bedrock = new Const { ConstantValue = 6f };

        Const constant1 = new Const { ConstantValue = 1 };
        Const constant0 = new Const { ConstantValue = 0 };

        
        // Gradient
        // Remember, the noise module uses xy = ground plane, z = vertical. so keep that in mind.  Y in unity = Z in Gradient.
        Aubergine.Noise.Module.Gradient mainGradient = new Aubergine.Noise.Module.Gradient(0, 0, .5, 0, 0, 0);
        ScaleBias mainGradientRemap = new ScaleBias(mainGradient, 0.5, 0.5);

        // Semi-Rare ores
        Perlin semiRarePerlin = new Perlin { OctaveCount = 4, Frequency = 2 };
        ScaleBias semiRarePerlinRemap = new ScaleBias(semiRarePerlin, 0.5f, 0.5f);
        Select semiRareSelect = new Select(semiRare, stone, semiRarePerlinRemap);
        // stone is chosen when the number falls between Semirare density and 1000. Smaller numbers numbers = less semi-rare.
        semiRareSelect.SetBounds(SemiRareDensity, 1000);


        // Rare ores
        Perlin rarePerlin = new Perlin { OctaveCount = 3, Frequency = 3 };
        ScaleBias rarePerlinRemap = new ScaleBias(rarePerlin, 0.5f, 0.5f);
        ScaleBias rarePerlinScale = new ScaleBias(rarePerlinRemap, RareGradientScale, 0);
        Multiply rareMulti = new Multiply(rarePerlinScale, mainGradientRemap);
        ScaleBias rareMultiScale = new ScaleBias(rareMulti, RareDensity, 0);
        Select rareSelect = new Select(semiRareSelect, rare, rareMultiScale);
        rareSelect.SetBounds(.5, 1000);


        // Dirt Layer
        Select dirtStoneSelect = new Select(dirt, rareSelect, mainGradientRemap);
        dirtStoneSelect.SetBounds(DirtDepth, 1000);

        // set anything above dirtlayer as air.
        Select groundSelect = new Select(open, dirtStoneSelect, mainGradientRemap);
        groundSelect.SetBounds(0.000001, 1000);


        //-----------------------------------------------Applying Ground Shape-----------------------------------------------

        // Mountain Shape.
        Perlin mountainShape = new Perlin { Frequency = 2, Lacunarity = 1.4, OctaveCount = 3 };
        ScaleBias mountainBias = new ScaleBias(mountainShape,.75,-.25);
        ScalePoint mountainZScale = new ScalePoint(mountainBias) { ZScale = GroundZScale };

        // Hills Shape
        Perlin hillsShape = new Perlin { OctaveCount = 4, Frequency = 2,Persistence = .2,Lacunarity = 1.1,Seed = GroundSeed };
        ScaleBias hillsBias = new ScaleBias(hillsShape,.25,0);
        ScalePoint hillZScale = new ScalePoint(hillsBias) { ZScale = GroundZScale };
        

        // Planes Shape
        Billow flatTerrain = new Billow{Frequency = 2,OctaveCount = 1, Lacunarity = 1};
        ScaleBias flatTerrainBias = new ScaleBias(flatTerrain,0.05,0);
        ScalePoint flatTerrainScale = new ScalePoint(flatTerrainBias) { ZScale = GroundZScale };

        // Selectors (noise to determine which terrain to use and where
        Perlin SelectorOne = new Perlin{Frequency = 1, Persistence = 0.25, Seed = 24240};

        Select mountainAndHills = new Select(hillZScale, mountainZScale, SelectorOne);
        mountainAndHills.SetBounds(MountainsVsHills, 1000);
        mountainAndHills.EdgeFallOff = EdgeFallOff;

        Select hillsAndPlanes = new Select(flatTerrainScale, mountainAndHills, SelectorOne);
        hillsAndPlanes.SetBounds(MountainhillsVsPlains, 1000);
        hillsAndPlanes.EdgeFallOff = EdgeFallOff;

        // Now we apply the turbulance that the selectors generated.
        ExtendedTurbulence finalTurb = new ExtendedTurbulence(groundSelect);
        finalTurb.SetConstantTurb(constant0);
        finalTurb.ZDistortModule = hillsAndPlanes;
        finalTurb.Power = 0.5f;
        

 

        //-----------------------------------------------Bringing back the caves-----------------------------------------------
        RidgedMulti caveShape1 = new RidgedMulti { Frequency = 2, OctaveCount = 1 };
        Select caveBase1 = new Select(constant0, constant1, caveShape1);
        caveBase1.SetBounds(CaveSize, 1000);

        RidgedMulti caveShape2 = new RidgedMulti { Frequency = 2, OctaveCount = 1, Seed = 1323 };
        Select caveBase2 = new Select(constant0, constant1, caveShape2);
        caveBase1.SetBounds(1 - CaveSize, 1000);
        Multiply caveMult = new Multiply(caveBase1, caveBase2);

        Perlin caveTurbX = new Perlin { OctaveCount = 3, Frequency = 3, Seed = 1001 };
        Perlin caveTurbY = new Perlin { OctaveCount = 3, Frequency = 3, Seed = 1201 };
        Perlin caveTurbZ = new Perlin { OctaveCount = 3, Frequency = 3, Seed = 1301 };
        ExtendedTurbulence caveTurb = new ExtendedTurbulence(caveMult, caveTurbX, caveTurbY, caveTurbZ);
        caveTurb.Power = .25;

        ScaleBias caveInvert = new ScaleBias(caveTurb, -1, 1);
        Select caveSelect = new Select(open, finalTurb, caveInvert, 0, .5, 1000);

        // Draw the bedrock, so there's less people falling into the abyss :)
        Select bedrockSelect;
        if (!DisableCaves)
        {
            bedrockSelect = new Select(caveSelect, bedrock, mainGradientRemap);
            bedrockSelect.SetBounds(BedrockThreshold, 1000);
        }
        else
        {
            bedrockSelect = new Select(finalTurb, bedrock, mainGradientRemap);
            bedrockSelect.SetBounds(BedrockThreshold, 1000);
        }

        // If a map already exists, destory it and then send this off to the generator to draw the map in voxels.
        if (existingMap) DestroyObject(existingMap);

        if (CreateCubicVolume)
        {
            existingMap = CubiquityGenerator.Generate(bedrockSelect, new Vector3(MapDepth, MapHeight, MapWidth), DisableRocks, DisableIncrementalBuild);
        }
        else
        {
            existingMap = CubiquityVolumeTerrainBuilder.GenerateVolume(bedrockSelect, new Vector3(MapDepth, MapHeight, MapWidth), DisableRocks, DisableIncrementalBuild, SmoothNoncubicTerrain);
        }

    }

    void OnGUI()
    {

        if (GUI.Button(new Rect(0, 0, 150, 30), "Regenerate Map"))
            BuildMap();
        GUI.TextField(new Rect(155, 0, 200,30), "Built Terrain in: " +TimetoBuild.ToString() + " seconds.");
        if (textureToRender != null)
            GUI.DrawTexture(new Rect(0, 30, textureToRender.width, textureToRender.height), textureToRender, ScaleMode.ScaleToFit, true);

    }

    private void CreateTexture(IModule suppliedNoise)
    {
        // Create a terrain map.
        
        NoiseMapBuilderPlane terrainBuilder = new NoiseMapBuilderPlane(MapWidth, MapDepth);
        terrainBuilder.SetBounds(0, MapWidth/64f, 0, MapDepth/64f);
        terrainBuilder.Build(suppliedNoise);


        float mountainSize;
        float hillsSize;
        float hillsandMountainsSize;
        float plainsSize;
        plainsSize = MountainhillsVsPlains + 1;

        hillsandMountainsSize = 2 - plainsSize;
        mountainSize = (((MountainsVsHills + 1)/2)*hillsandMountainsSize);
        hillsSize = hillsandMountainsSize - mountainSize;


        // Build a renderer (to show the terrain that is to be built)
        RendererImage render = new RendererImage();
        render.SourceNoiseMap = terrainBuilder.Map;
        render.ClearGradient();
        render.AddGradientPoint(-1.0000, (Color32)Color.black);
        render.AddGradientPoint(mountainSize, (Color32)Color.red);
        render.AddGradientPoint(hillsSize, (Color32)Color.blue);
        render.AddGradientPoint(1.0000, (Color32)Color.white);

        

        render.Render();

        textureToRender = render.GetTexture();
        return;
    }
}
