﻿using Cubiquity;
using UnityEngine;
using System.Collections;

public class CopyVolumeTest : MonoBehaviour {

	// Use this for initialization
	void Start () {
	RunTest();
	}
	
	// Update is called once per frame
	void RunTest () {
        Region myRegion = new Region(0, 0, 0, 20,20,20);

        TerrainVolumeData data = TerrainVolumeData.CreateEmptyVolumeData<TerrainVolumeData>(myRegion);
        string volumeDataPath = data.fullPathToVoxelDatabase;
        // Create some ground in the terrain so it shows up in the editor.
        // Soil as a base (mat 1) and then a couple of layers of grass (mat 2).
        //TerrainVolumeGenerator.GenerateFloor(data, 2, (uint)1, 3, (uint)2);

        // Now create the terrain game object from the data.
        GameObject terrain = TerrainVolume.CreateGameObject(data,true,true);

        // Set up our material	
        Material material = new Material(Shader.Find("TriplanarTexturing"));
        terrain.GetComponent<TerrainVolumeRenderer>().material = material;

        // Set up the default textures
        Texture2D rockTexture = Resources.Load("Textures/Rock (Basic)") as Texture2D;
        Texture2D soilTexture = Resources.Load("Textures/GoodDirt") as Texture2D;
        Texture2D grassTexture = Resources.Load("Textures/Grass (Meadows2)") as Texture2D;
        Texture2D goldTexture = Resources.Load("Textures/Sand (with pebbles)") as Texture2D;
        Texture2D ironOreTexture = Resources.Load("Textures/IronOre") as Texture2D;

        // It's possible the textures won't actually be found, as they are just examples and the
        // user might have decided not to include them when importing Cubiquity. This doesn't
        // matter and just means the uer will have to set up their own textures.
        if (rockTexture != null && soilTexture != null && grassTexture != null)
        {
            material.SetTexture("_Tex0", rockTexture);
            material.SetTextureScale("_Tex0", new Vector2(0.062f, 0.062f));
            material.SetTexture("_Tex1", soilTexture);
            material.SetTextureScale("_Tex1", new Vector2(0.125f, 0.125f));
            material.SetTexture("_Tex2", grassTexture);
            material.SetTextureScale("_Tex2", new Vector2(0.125f, 0.125f));
            material.SetTexture("_Tex3", goldTexture);
            material.SetTextureScale("_Tex3", new Vector2(0.125f, 0.125f));
            material.SetTexture("_Tex4", ironOreTexture);
            material.SetTextureScale("_Tex4", new Vector2(0.125f, 0.125f));

        }

        // Setup the materials
        MaterialSet materialSet = new MaterialSet();
        // Ok, lets get a-buildin!

        // This scale factor comtrols the size of the rocks which are generated.
        float inverseScale = 1.0f / 32;

        int height = myRegion.upperCorner.y;
        int depth = myRegion.upperCorner.z;
        int width = myRegion.upperCorner.x;
        // Iterate over every voxel of our volume
        for (int x = 0; x < width; x++)
        {
            for (int z = 0; z < depth; z++)
            {
                for (int y = 0; y < height; y++)
                {

                    // Make sure we don't have anything left in here from the previous voxel
                    materialSet.weights[0] = 0;
                    materialSet.weights[1] = 0;
                    materialSet.weights[2] = 0;
                    materialSet.weights[3] = 0;
                    materialSet.weights[4] = 0;
  
                    //Set the material based upon the return above.
                    materialSet.weights[0] = (byte)255;
                    // write the cube.
                    data.SetVoxel(x, y, z, materialSet);
                }
            }
        }

        // Save the volume.
        data.CommitChanges();
	    
        TerrainVolumeData cloneData = TerrainVolumeData.CreateFromVoxelDatabase<TerrainVolumeData>(volumeDataPath);

        // Create some ground in the terrain so it shows up in the editor.
        // Soil as a base (mat 1) and then a couple of layers of grass (mat 2).
        //TerrainVolumeGenerator.GenerateFloor(data, 2, (uint)1, 3, (uint)2);

        // Now create the terrain game object from the data.
        GameObject cloneTerrain = TerrainVolume.CreateGameObject(cloneData,true,true);
        cloneTerrain.GetComponent<TerrainVolumeRenderer>().material = material;
        cloneTerrain.transform.position = new Vector3(0,0,30);



	}
}
